using Helpers.Configurations;
using MessageQueueProcessor;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
    {
        IConfiguration configuration = hostContext.Configuration;
        services.Configure<RabbitMQConfiguration>(configuration.GetSection(nameof(RabbitMQConfiguration)));
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
