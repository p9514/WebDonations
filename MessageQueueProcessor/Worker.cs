using Helpers.Configurations;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Services.Models.Donations;
using System.Text;
using MailKit;
using MailKit.Net.Smtp;
using MimeKit;

namespace MessageQueueProcessor
{
    public class Worker : BackgroundService
    {
        private readonly RabbitMQConfiguration _configuration;

        public Worker(IOptions<RabbitMQConfiguration> configuration)
        {
            _configuration = configuration.Value;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var factory = new ConnectionFactory { HostName = _configuration.HostName };
            var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "donationCompletedEmail",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, eventArgs) =>
            {
                var body = eventArgs.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var emailDetails = JsonConvert.DeserializeObject<SendDonationCompletedEmail>(message);
                SendDonationCompletedEmail(emailDetails);
            };

            channel.BasicConsume(queue: "donationCompletedEmail", autoAck: true, consumer: consumer);
            while(true) { }
        }

        private string CreateEmailBody(SendDonationCompletedEmail emailDetails)
        {
            string productsList = "";

            foreach (var product in emailDetails.Products)
            {
                productsList += "<li> " +
                    product.ProductName + ": " +
                    product.Quantity.ToString() + " " + product.MeasureUnit +
                    "</li>";
            }

            return String.Format
                (@"<h1> Donatie confirmata </h1>
                <br>

                <h3> Buna ziua,</h3>
                <br>
                <p> Produsele donate sunt urmatoarele:</p>
                <ul>" +

                productsList +

                @"</ul>
                <br>


                <p> Acestea trebuie ridicate de la adresa - "
                + emailDetails.UserAddress?.City.ToString() +
                ", strada " + emailDetails.UserAddress?.Street.ToString() +
                ", numarul " + emailDetails.UserAddress?.Number.ToString() +
                ", " + emailDetails.UserAddress?.Country.ToString() +
                " - si livrate la adresa - "
                + emailDetails.CenterAddress?.City.ToString() +
                ", strada " + emailDetails.CenterAddress?.Street.ToString() +
                ", numarul " + emailDetails.CenterAddress?.Number.ToString() +
                ", " + emailDetails.CenterAddress?.Country.ToString() + "." +

                @"</p>
                <br>


                <p> Multumim </p>");
        }

        private void SendDonationCompletedEmail(SendDonationCompletedEmail emailDetails)
        {

            MimeMessage message = new MimeMessage();

            var bodyBuilder = new BodyBuilder();

            bodyBuilder.HtmlBody = CreateEmailBody(emailDetails);

            message.From.Add(new MailboxAddress("WebDonations", "web.donations.mail@gmail.com"));

            string courierEmail = "nedelcu.stefan17@yahoo.com";

            message.To.Add(MailboxAddress.Parse(courierEmail));

            message.Subject = "Livrare donatie";

            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();

            try
            {
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate("web.donations.mail@gmail.com", "Webdonations91");
                client.Send(message);
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }
    }
}