﻿namespace Services.Interfaces;

public interface IMetricReporter
{
    public void RegisterDonation();

    public void RegisterDonationRequest();

    public void RegisterUser();
}
