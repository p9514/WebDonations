﻿using Database.Entities;
using Services.Models.Users;

namespace Services.Interfaces;

public interface IUsersService
{
    IEnumerable<UserModel> Index();

    UserDetails GetUserProfile(long userId);

    UserContext GetUserContext(string userEmail);

    void UpdateUserProfile(UpdateUserRequest req);

    Address? GetUserAddress(long userId);
}