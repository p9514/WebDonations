﻿using Services.Models.Donations;

namespace Services.Interfaces;

public interface IDonationsService
{
    IEnumerable<DonationRequestDetails> GetDonationRequests();

    DonationRequestDetails GetDonationRequest(long id);

    void PostDonationRequest(DonationRequestDetailsRequest req);

    IEnumerable<DonationRequestDetails> GetCenterDonationRequests(long centerUserId);

    void UpdateDonationRequest(UpdateDonationRequest req);

    void DonateProducts(DonateProductsRequest req);

    IEnumerable<Product> GetDonationRequestProducts(long requestId);

    long UpdateProductRemainingNecesaryQuantity(long requestProductId, int donatedQuantity);

    void UpdateDonationRequestStatus(long donationRequestId);

    long GetDonationCenterId(long donationRequestId);

    List<Product> GetProductsDetails(DonateProductsRequest req);
}