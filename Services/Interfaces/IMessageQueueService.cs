﻿namespace Services.Interfaces;

public interface IMessageQueueService
{
    void SendMessage<T>(T task, string queueName);
}
