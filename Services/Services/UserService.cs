﻿using Database.Entities;
using Helpers.Enums;
using Microsoft.EntityFrameworkCore;
using Services.Models.Users;
using Services.Interfaces;

namespace Services.Services
{
    public class UsersService : IUsersService
    {
        private readonly WDDbContext _context;
        private readonly IMetricReporter _metricReporter;

        public UsersService(WDDbContext context, IMetricReporter metricReporter)
        {
            _context = context;
            _metricReporter = metricReporter;
        }

        public IEnumerable<UserModel> Index()
        {
            return _context.Users
                .Select(u => new UserModel
                {
                    IdUser = u.IdUser,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Phone = u.Phone,
                    UserRole = u.RoleId.AsEnum<UserRole>().GetEnumDescription(),
                })
                .OrderBy(u => u.LastName)
                .ToList();
        }

        public UserDetails GetUserProfile(long userId)
        {
            var userAddress = _context.UserAddresses
                .Where(a => a.IdUser == userId)
                .Include(a => a.User)
                .FirstOrDefault();
            if (userAddress == null)
            {
                throw new Exception($"User {userId} not found");
            }

            return new UserDetails
            {
                UserId = userId,
                Email = userAddress.User.Email,
                FirstName = userAddress.User.FirstName,
                LastName = userAddress.User.LastName,
                PhoneNumber = userAddress.User.Phone,
                Role = userAddress.User.RoleId.AsEnum<UserRole>().GetEnumDescription(),
                Country = userAddress?.Country,
                Street = userAddress?.Street,
                City = userAddress?.City,
                Number = userAddress?.Number,
            };
        }

        public UserContext GetUserContext(string userEmail)
        {
            var user = _context.Users
                .Where(u => u.Email == userEmail)
                .FirstOrDefault();
            if (user == null)
            {
                var newUserId = CreateEmptyUser(userEmail);
                return new UserContext
                {
                    UserId = newUserId,
                    Role = "Donor"
                };
            }

            return new UserContext
            {
                UserId = user.IdUser,
                Role = user.RoleId.AsEnum<UserRole>().GetEnumDescription(),
            };
        }

        private long CreateEmptyUser(string userEmail)
        {
            _metricReporter.RegisterUser();

            var newUser = new User
            {
                Email = userEmail,
                FirstName = string.Empty,
                LastName = string.Empty,
                Phone = string.Empty,
                RoleId = (int)UserRole.Donor,
            };

            var newAddress = new UserAddress
            {
                User = newUser,
                Country = string.Empty,
                City = string.Empty,
                Street = string.Empty,
                Number = string.Empty,
            };

            _context.Users.Add(newUser);
            _context.UserAddresses.Add(newAddress);
            _context.SaveChanges();

            return newUser.IdUser;
        }

        public void UpdateUserProfile(UpdateUserRequest req)
        {
            var userAddress = _context.UserAddresses
                .Where(a => a.IdUser == req.UserId)
                .Include(a => a.User)
                .FirstOrDefault();
            if (userAddress == null)
            {
                throw new Exception($"User {req.UserId} not found");
            }

            userAddress.User.FirstName = req.FirstName;
            userAddress.User.LastName = req.LastName;
            userAddress.User.Phone = req.PhoneNumber;
            userAddress.Country = req.Country;
            userAddress.City = req.City;
            userAddress.Street = req.Street;
            userAddress.Number = req.Number;

            _context.SaveChanges();
        }

        public Address? GetUserAddress(long userId)
        {
            return _context.UserAddresses
                .Where(u => u.IdUser == userId)
                .Select(u => new Address
                {
                    Country = u.Country,
                    City = u.City,
                    Street = u.Street,
                    Number = u.Number,
                })
                .FirstOrDefault();
        }
    }
}
