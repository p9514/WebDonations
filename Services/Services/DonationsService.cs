﻿using Database.Entities;
using Helpers.Enums;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using Services.Models.Donations;

namespace Services.Services
{
    public class DonationsService : IDonationsService
    {
        private readonly WDDbContext _context;
        private readonly IUsersService _usersService;
        private readonly IMessageQueueService _messageQueueService;

        public DonationsService(WDDbContext context, IUsersService userService, IMessageQueueService messageQueueService)
        {
            _context = context;
            _usersService = userService;
            _messageQueueService = messageQueueService;
        }

        public IEnumerable<Product> GetDonationRequestProducts(long requestId)
        {
             return _context.DonationRequestProducts
                .Where(v => v.IdRequest == requestId)
                .Select(v => new Product
                {
                    RequestProductId = v.IdRequestProduct,
                    ProductName = v.ProductName,
                    Quantity = v.Quantity,
                    MeasureUnit = v.MeasureUnit,
                })
                .ToList();
        }
        
        public IEnumerable<DonationRequestDetails> GetDonationRequests()
        {
            return _context.DonationRequests
                .Include(u => u.CenterUser)
                .ToList()
                .Select(ToDonationRequestDetails)
                .OrderByDescending(r => r.CreatedAt)
                .ToList();
        }

        public DonationRequestDetails GetDonationRequest(long id)
        {
            var request = _context.DonationRequests
                .Where(r => r.IdRequest == id)
                .Include(r => r.CenterUser)
                .FirstOrDefault();
            if (request == null)
                throw new Exception($"Donation request {id} not found");

            return ToDonationRequestDetails(request);
        }

        public void PostDonationRequest(DonationRequestDetailsRequest req)
        {
            var products = req.ProductsRemained.ToList();
            
            var donationRequest = new DonationRequest
            {
                IdCenterUser = req.CenterUserId,
                CreatedAt = DateTime.Now,
                AvailableTo = DateTime.Now.AddMonths(1),
                Status = "Available"
            };

            var donationRequestProducts = products
                .Select(p => new DonationRequestProduct
                {
                    Request = donationRequest,
                    ProductName = p.ProductName,
                    Quantity = p.Quantity,
                    MeasureUnit = p.MeasureUnit,
                })
                .ToList();

            _context.DonationRequests.Add(donationRequest);
            _context.DonationRequestProducts.AddRange(donationRequestProducts);
            _context.SaveChanges();
        }

        public IEnumerable<DonationRequestDetails> GetCenterDonationRequests(long centerUserId)
        {
            var user = _context.Users.Where(u => u.IdUser == centerUserId).FirstOrDefault();
            if (user == null)
                throw new Exception($"User {centerUserId} not found");

            if (user.RoleId != (int)UserRole.Center)
                throw new Exception($"You do not have permission to do this");

            return _context.DonationRequests
                .Where(r => r.IdCenterUser == centerUserId)
                .Include(r => r.CenterUser)
                .ToList()
                .Select(ToDonationRequestDetails)
                .ToList();
        }

        public void UpdateDonationRequest(UpdateDonationRequest req)
        {
            foreach(var product in req.Products)
            {
                var existingProduct = _context.DonationRequestProducts
                        .Where(p => p.IdRequestProduct == product.RequestProductId)
                        .FirstOrDefault();
                if (existingProduct == null)
                    continue;

                existingProduct.Quantity = product.Quantity;
            }
            _context.SaveChanges();
        }

        public long UpdateProductRemainingNecesaryQuantity(long requestProductId, int donatedQuantity)
        {
            var productRequest = _context.DonationRequestProducts
                                .Where(u => u.IdRequestProduct == requestProductId)
                                .Include(u => u.Request)
                                .FirstOrDefault();
            if (productRequest == null)
                throw new Exception($"Product {requestProductId} not found");

            if (donatedQuantity > productRequest.Quantity)
            {
                productRequest.Quantity = 0;
            }
            else
            {
                productRequest.Quantity -= donatedQuantity;
            }            

            _context.SaveChanges();

            return productRequest.IdRequest;
        }

        public void UpdateDonationRequestStatus(long donationRequestId)
        {
            var request = _context.DonationRequests
                        .Where(r => r.IdRequest == donationRequestId)
                        .Include(r => r.DonationRequestProducts)
                        .FirstOrDefault();
            if (request == null)
                throw new Exception($"Request {donationRequestId} not found");

            bool isCompleted = request.DonationRequestProducts
                .All(p => p.Quantity <= 0);
            request.Status = isCompleted ? "Completed" : "Available";

            _context.SaveChanges();
        }

        public long GetDonationCenterId(long donationRequestId)
        {
            return _context.DonationRequests
                .First(i => i.IdRequest == donationRequestId).IdCenterUser;
        }

        public List<Product> GetProductsDetails(DonateProductsRequest req)
        {
            var productsDetails = new List<Product>();
            foreach (var donatedProduct in req.DonatedProducts)
            {
                var productRequestDetail = _context.DonationRequestProducts
                            .Where(i => i.IdRequestProduct == donatedProduct.RequestProductId)
                            .FirstOrDefault();
                if(productRequestDetail == null)
                    continue;

                if (donatedProduct.Quantity == 0) continue;

                productsDetails.Add(new Product
                {
                    ProductName = productRequestDetail.ProductName,
                    Quantity = donatedProduct.Quantity,
                    MeasureUnit = productRequestDetail.MeasureUnit
                });
            }

            return productsDetails;
        }

        public void DonateProducts(DonateProductsRequest req)
        {
            long donationRequestId = 0;

            foreach(var product in req.DonatedProducts)
            {
                var userDonation = new UserDonationProduct
                {
                    IdDonorUser = req.DonorUserId,
                    IdRequestProduct = product.RequestProductId,
                    Quantity = product.Quantity
                };

                _context.UserDonationProducts.Add(userDonation);

                donationRequestId = UpdateProductRemainingNecesaryQuantity(userDonation.IdRequestProduct, userDonation.Quantity);
            }

            if(donationRequestId != 0)
            {
                UpdateDonationRequestStatus(donationRequestId);
            }

            _context.SaveChanges();

            _messageQueueService.SendMessage(new SendDonationCompletedEmail
            {
                UserAddress = _usersService.GetUserAddress(req.DonorUserId),
                CenterAddress = _usersService.GetUserAddress(GetDonationCenterId(donationRequestId)),
                Products = GetProductsDetails(req)
            }, "donationCompletedEmail");
        }

        private DonationRequestDetails ToDonationRequestDetails(DonationRequest req)
        {
            return new DonationRequestDetails
            {
                DonationRequestId = req.IdRequest,
                CenterName = $"{req.CenterUser.FirstName} {req.CenterUser.LastName}",
                CenterPhoneNumber = req.CenterUser.Phone,
                CreatedAt = req.CreatedAt,
                AvailableTo = req.AvailableTo,
                Status = req.Status,
                Address = _usersService.GetUserAddress(req.CenterUser.IdUser),
                ProductsRequired = GetDonationRequestProducts(req.IdRequest),
            };
        }
    }
}
