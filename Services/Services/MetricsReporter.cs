﻿using Services.Interfaces;
using Prometheus;
using Microsoft.Extensions.Logging;

namespace Services.Services
{
    public class MetricReporter : IMetricReporter
    {
        private readonly ILogger<MetricReporter> _logger;
        private readonly Counter _donationsCounter;
        private readonly Counter _donationRequestsCounter;
        private readonly Counter _usersCounter;

        public MetricReporter(ILogger<MetricReporter> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _donationsCounter = Metrics.CreateCounter("total_donations", "The total number of donations made by users.");
            _donationRequestsCounter = Metrics.CreateCounter("total_donationRequests", "The total number of donations made by users.");
            _usersCounter = Metrics.CreateCounter("total_users", "The total number of donations made by users.");
        }

        public void RegisterDonation()
        {
            _donationsCounter.Inc();
        }

        public void RegisterDonationRequest()
        {
            _donationRequestsCounter.Inc();
        }

        public void RegisterUser()
        {
            _usersCounter.Inc();
        }
    }
}
