﻿using Helpers.Configurations;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Services.Interfaces;
using System.Text;

namespace Services.Services
{
    public class MessageQueueService : IMessageQueueService
    {
        private readonly RabbitMQConfiguration _configuration;

        public MessageQueueService(IOptions<RabbitMQConfiguration> configuration)
        {
            _configuration = configuration.Value;
        }

        public void SendMessage<T>(T task, string queueName)
        {
            var factory = new ConnectionFactory { HostName = _configuration.HostName };
            var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: queueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var json = JsonConvert.SerializeObject(task);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange: string.Empty, routingKey: queueName, body: body);
        }
    }
}
