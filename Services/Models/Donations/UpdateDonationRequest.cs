﻿namespace Services.Models.Donations;

public class UpdateDonationRequest
{
    public List<Product> Products { get; set; }
}
