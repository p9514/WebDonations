﻿namespace Services.Models.Donations;

public class DonateProductsRequest
{
    public long DonorUserId { get; set; } = 0;
    public List<DonatedProduct> DonatedProducts { get; set; }
}
