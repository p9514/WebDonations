﻿using Services.Models.Users;
namespace Services.Models.Donations;

public class SendDonationCompletedEmail
{
    public Address? UserAddress { get; set; }

    public Address? CenterAddress { get; set; }

    public List<Product> Products { get; set; } = new();
}
