﻿namespace Services.Models.Donations;

public class DonatedProduct
{
    public long RequestProductId { get; set; } = 0;
    public int Quantity { get; set; } = 0;
}
