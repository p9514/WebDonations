﻿using Services.Models.Users;

namespace Services.Models.Donations;

public class DonationRequestDetails
{
    public long? DonationRequestId { get; set; }
    public string CenterName { get; set; } = string.Empty;
    public string CenterPhoneNumber { get; set; } = string.Empty;
    public DateTime CreatedAt { get; set; }
    public DateTime AvailableTo { get; set; }
    public string Status { get; set; } = string.Empty;
    public Address? Address { get; set; }
    public IEnumerable<Product> ProductsRequired { get; set; }
}
