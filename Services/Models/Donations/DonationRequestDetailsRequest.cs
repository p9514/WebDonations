﻿namespace Services.Models.Donations;

public class DonationRequestDetailsRequest
{
    public long CenterUserId { get; set; } = 0;
    public IEnumerable<Product> ProductsRemained { get; set; }
}
