﻿namespace Services.Models.Donations;

public class Product
{
    public long? RequestProductId { get; set; }
    public string ProductName { get; set; } = string.Empty;
    public long Quantity { get; set; }
    public string MeasureUnit { get; set; } = string.Empty;

}
