namespace Services.Models.Users;

public class UserModel
{
    public long IdUser { get; set; }
    public string Email { get; set; } = string.Empty;
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;
    public string UserRole { get; set; } = string.Empty;
}
