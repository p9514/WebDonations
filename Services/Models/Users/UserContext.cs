﻿using System.ComponentModel.DataAnnotations;

namespace Services.Models.Users;

public class UserContext
{
    [Required]
    public long UserId { get; set; }

    [Required]
    public string Role { get; set; } = string.Empty;
}