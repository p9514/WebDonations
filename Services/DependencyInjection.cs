﻿using Database.Entities;
using Microsoft.Extensions.DependencyInjection;
using Services.Interfaces;
using Services.Services;

namespace Services;

public static class DependencyInjection
{
    public static IServiceCollection AddDataServices(this IServiceCollection services)
    {
        services.AddTransient<IUsersService, UsersService>();
        services.AddTransient<IDonationsService, DonationsService>();
        services.AddTransient<IMessageQueueService, MessageQueueService>();
        services.AddTransient<IMetricReporter, MetricReporter>();

        return services;
    }

    public static IServiceCollection AddWDDbContext(this IServiceCollection services)
    {
        services.AddDbContext<WDDbContext>();       

        return services;
    }
}
