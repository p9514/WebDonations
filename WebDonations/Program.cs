using Helpers.Configurations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Services;
using Prometheus;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddDataServices();
builder.Services.AddWDDbContext();
builder.Services.AddSwaggerGen();

builder.Services.Configure<ConnectionStrings>(builder.Configuration.GetSection("ConnectionStrings"));
builder.Services.Configure<RabbitMQConfiguration>(builder.Configuration.GetSection("RabbitMQConfiguration"));

builder.Services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer(options =>
    {
        options.Authority = "https://dev-x9tksl4b.eu.auth0.com/";
        options.Audience = "https://webdonations.com";
    });

builder.Services.AddAuthorization(configure =>
{
    configure.AddPolicy("DonationCenter", policy => policy.RequireClaim("permissions", "webdonations:donation-center"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebDonations API");
        c.RoutePrefix = "swagger";
    });
}

app.UseStaticFiles();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.UseHttpMetrics();
app.UseMetricServer();


app.MapControllerRoute(
    name: "default",
    pattern: "api/{controller}/{action}/{id?}");

app.UseEndpoints(endpoints =>
{
    endpoints.MapMetrics();
});

app.MapFallbackToFile("index.html");

app.Run();
