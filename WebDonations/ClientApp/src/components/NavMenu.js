import React, { useState, useContext } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';
import { userContext } from '../config/userContext';

import './NavMenu.css';

const NavMenu = (props) => {
  const [ collapsed, toggleCollapsed ] = useState(true);
  const { logout, isAuthenticated } = useAuth0();
  const userData = useContext(userContext);

  return (
    <header>
      <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
        <Container>
          <NavbarBrand tag={Link} to="/">WebDonations</NavbarBrand>
          <NavbarToggler onClick={() => toggleCollapsed(!collapsed)} className="mr-2" />
          <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!collapsed} navbar>
            <ul className="navbar-nav flex-grow">
              <NavItem>
                <NavLink tag={Link} className="text-dark" to="/">Profile</NavLink>
              </NavItem>
              { (userData && userData.role === "Donations Center") &&
              (<NavItem>
                <NavLink tag={Link} className="text-dark" to="/createDonation">Cerere</NavLink>
              </NavItem>)
              }
              <NavItem>
                <NavLink tag={Link} className="text-dark" to="/requests">Doneaza</NavLink>
              </NavItem>
              {isAuthenticated && (
              <NavItem>
                <NavLink onClick={() => logout()} className="text-dark" style={{cursor:'pointer'}}>Logout</NavLink>
              </NavItem>
              )}
            </ul>
          </Collapse>
        </Container>
      </Navbar>
    </header>
  );
}

export default NavMenu;
