import React, { useEffect, useState, useContext } from 'react';
import { Button, Form, FormGroup, Label, Input, Card, Container, Row, Col, Alert } from 'reactstrap';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import Loading from './Loading';
import { useAuth0 } from "@auth0/auth0-react";
import axiosInstance from "../config/Axios";
import { userContext } from '../config/userContext';


const Profile = () => {
  const { getAccessTokenSilently } = useAuth0();

  const [ profile, setProfile ] = useState();
  const [ isSuccess, setIsSuccess ] = useState(false);
  const userData = useContext(userContext);
  
  useEffect(() => {
    populateProfile();
  }, [userData]);

  const handleChange = (e) => {
    setProfile({ ...profile, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    updateProfile();
  };

  return (
    <> 
    {profile && (
      <Card>        
        <Form className='mx-5 mt-5 mb-5' onSubmit={handleSubmit}>
          <h3 className='text-center mb-4'>
            Va rugam sa va actualizati profilul pentru a putea folosi functionalitatile platformei noastre.
          </h3>
          <Container>
            <Row>
              <Col xs="12" md="6">
                <h5>Date personale</h5>
                <FormGroup className='mb-4'>
                  <Label for="exampleEmail">Adresa de email:</Label>
                  <Input type="email" name="email" id="exampleEmail" placeholder="Va rugam introduceti adresa de email" 
                    value={profile.email} disabled />
                </FormGroup>
                <FormGroup className='mb-4'>
                  <Label for="exampleLastName">Nume</Label>
                  <Input type="text" name="lastName" id="exampleLastName" placeholder="Ionescu" 
                    value={profile.lastName}
                    onChange={handleChange} 
                    required />
                </FormGroup>
                <FormGroup className='mb-4'>
                  <Label for="exampleFirstName">Prenume</Label>
                  <Input type="text" name="firstName" id="exampleFirstName" placeholder="Ion" 
                    value={profile.firstName}
                    onChange={handleChange} 
                    required />
                </FormGroup>
                <FormGroup className='mb-4'>
                  <Label for="examplePhone">Numar de telefon:</Label>
                  <Input type="text" name="phoneNumber" id="examplePhone" placeholder="+40 763 123 123" 
                    value={profile.phoneNumber}
                    onChange={handleChange} 
                    required />
                </FormGroup>
              </Col>
              <Col xs="12" md="6">
                <h5>Adresa</h5>
                <FormGroup className='mb-4'>
                  <Label for="exampleCountry">Tara</Label>
                  <Input type="text" name="country" id="exampleCountry" placeholder="Romania" 
                    value={profile.country} 
                    onChange={handleChange} 
                    required />
                </FormGroup>
                <FormGroup className='mb-4'>
                  <Label for="exampleCity">Oras</Label>
                  <Input type="text" name="city" id="exampleCity" placeholder="Bucuresti" 
                    value={profile.city} 
                    onChange={handleChange}
                    required />
                </FormGroup>
                <FormGroup className='mb-4'>
                  <Label for="exampleStreet">Strada</Label>
                  <Input type="text" name="street" id="exampleStreet" placeholder="Iasomiei" 
                    value={profile.street} 
                    onChange={handleChange}
                    required />
                </FormGroup>

                <FormGroup className='mb-4'>
                  <Label for="exampleStreetNumber">Numarul</Label>
                  <Input type="text" name="number" id="exampleStreetNumber" placeholder="25" 
                    value={profile.number} 
                    onChange={handleChange}
                    required />
                </FormGroup>
              </Col>
            </Row>

            <h6>Rolul dumneavoastra: {profile.role}</h6>

            <Button type="submit" color="primary">Salvati</Button>

            <Alert className='mt-4' color="success" isOpen={isSuccess} >
              Modificarile au avut loc cu succes
            </Alert>
          </Container>
        </Form>
      </Card>
    )}
    </>
  );

  async function populateProfile() {
    if (!userData) return;

    const accessToken = await getAccessTokenSilently();
    axiosInstance.get(`user/${userData.userId}/profile`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(({ data }) => {
        setProfile(data);
      });
  }

  async function updateProfile() {
    const accessToken = await getAccessTokenSilently();
    axiosInstance.post(`user`, profile, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(() => {
        setIsSuccess(true);
      });
  }
}

export default withAuthenticationRequired(Profile, {
  onRedirecting: () => <Loading />,
});