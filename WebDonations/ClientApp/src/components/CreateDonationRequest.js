import React, { useState, useContext } from 'react';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import Loading from './Loading';
import { userContext } from '../config/userContext';
import { useAuth0 } from "@auth0/auth0-react";
import { Button, Form, FormGroup, Label, Input, Card, Container, Row, Col, Alert } from 'reactstrap';
import axiosInstance from "../config/Axios";

const CreateDonationRequest = (props) => {
  const userData = useContext(userContext);
  const { getAccessTokenSilently } = useAuth0();  
  const [ isSuccess, setIsSuccess ] = useState(false);
  const [ products, setProducts ] = useState([]);
  const [ crtProduct, setCrtProduct ] = useState({ productName: '', quantity: 0, measureUnit: ''});

  const handleSubmit = (e) => {
    e.preventDefault();
    addDonation();
  };

  const addElement = () => {
    setProducts([...products, crtProduct]);
    setCrtProduct({ productName: '', quantity: 0, measureUnit: ''});
    setIsSuccess(false);
  }

  const handleChange = (e) => {
    setCrtProduct({ ...crtProduct, [e.target.name]: e.target.value });
  };

  return (
    <Card>        
      <Form className='mx-5 mt-5 mb-5' onSubmit={handleSubmit}>
        <h3 className='text-center mb-4'>
          Va rugam sa adaugati produsele de care este nevoie la centrul dumneavoastra.
        </h3>
        <Container>          
          <Row>
            <Col md='5' xs='12'>
                <FormGroup className='mb-4'>
                  <Label for="exampleProductName">Numele produsului:</Label>
                  <Input type="text" name="productName" id="exampleProductName" placeholder="Paine" 
                    value={crtProduct.productName}
                    onChange={handleChange} />
                </FormGroup>
            </Col>
            <Col md='2' xs='6'>
                <FormGroup className='mb-4'>
                  <Label for="exampleQuantity">Cantitatea:</Label>
                  <Input type="number" name="quantity" id="exampleQuantity" placeholder="5" 
                    value={crtProduct.quantity}
                    onChange={handleChange} />
                </FormGroup>
            </Col>
            <Col md='3' xs='6'>
                <FormGroup className='mb-4'>
                  <Label for="exampleMeasureUnit">Unitatea de masura:</Label>
                  <Input type="text" name="measureUnit" id="exampleMeasureUnit" placeholder="Bucati" 
                    value={crtProduct.measureUnit}
                    onChange={handleChange} />
                </FormGroup>
            </Col>
            <Col md='2' xs='3'>
              <Button onClick={() => addElement()} color="success" className='mb-4 mt-4'>Adaugati</Button>
            </Col>
          </Row>
          {
            products.length > 0 && (
              <>
              <h3>Produse adaugate:</h3>
              <ul>           
              {products.map(p =>
                <li key={p.productName}>
                  {p.productName}: {p.quantity} {p.measureUnit}
                </li>
              )}
              </ul>
              </>
            )
          }

          <Button type="submit" color="primary" className='mt-4'>Postati cererea</Button>

          <Alert className='mt-4' color="success" isOpen={isSuccess} >
            Modificarile au avut loc cu succes
          </Alert>
        </Container>
      </Form>
    </Card>
  );

  async function addDonation() {
    const accessToken = await getAccessTokenSilently();
    var param = { centerUserId: userData.userId, productsRemained: products }

    axiosInstance.post(`donation`, param, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(() => {
        setProducts([]);
        setCrtProduct({ productName: '', quantity: 0, measureUnit: ''});
        setIsSuccess(true);
      });
  }
}

export default withAuthenticationRequired(CreateDonationRequest, {
  onRedirecting: () => <Loading />,
});
