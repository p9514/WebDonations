import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import Loading from './Loading';
import axiosInstance from "../config/Axios";
import { useAuth0 } from "@auth0/auth0-react";
import { Card, Table } from 'reactstrap';


const DonationRequests = (props) => {
  const { getAccessTokenSilently } = useAuth0();
  const [donationRequests, setDonationRequests] = useState([]);  

  useEffect(() => {
    populateDonationRequests();
  }, [])

  return (
    <Card>
      <h3 className='text-center mt-4'>
        Aici puteti urmari produsele necesare la diverse centre de donatii din tara.
      </h3>
      <div className='mx-5 mt-4'>
      <Table>
        <thead>
          <tr>
            <th>Reprezentant centru</th>
            <th>Oras</th>
            <th>Statusul cererii</th>
            <th>Termen limita</th>
            <th>Produse necesare</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        {donationRequests.map(r =>
          <tr key={r.donationRequestId} style={{verticalAlign: 'middle'}}>
            <td>{r.centerName}</td>
            <td>{r.address.city}</td>
            <td>{r.status}</td>
            <td>{r.availableTo}</td>
            <td>
              <ul>
                {r.productsRequired.map(p =>
                  <li key={p.requestProductId}>{p.productName} - {p.quantity} {p.measureUnit}</li>
                )}
              </ul>
            </td>
            <td>
              <Link to={`/donate/${r.donationRequestId}`}>Doneaza acum!</Link>
            </td>
          </tr>
        )}
        </tbody>
      </Table>
      <h4 className='text-center mt-4 mb-5'>
        Va multumim pentru efortul depus!
      </h4>
      </div>
    </Card>
  );

  async function populateDonationRequests() {
    const accessToken = await getAccessTokenSilently();
    axiosInstance.get('donation', {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(({ data }) => {
        setDonationRequests(data);
      });
  }
}

export default withAuthenticationRequired(DonationRequests, {
  onRedirecting: () => <Loading />,
});
