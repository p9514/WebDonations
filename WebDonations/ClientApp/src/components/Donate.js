import React, { useEffect, useState, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import Loading from './Loading';
import { useAuth0 } from "@auth0/auth0-react";
import { Card, Button, Alert } from 'reactstrap';
import axiosInstance from "../config/Axios";
import { userContext } from '../config/userContext';


const Donate = (props) => {
  const { getAccessTokenSilently } = useAuth0();
  const { donationRequestId } = useParams();
  const userData = useContext(userContext);

  const [ donationRequest, setDonationRequest ] = useState();
  const [ requiredProducts, setRequiredProducts ] = useState([]);
  const [ isSuccess, setIsSuccess ] = useState(false);

  useEffect(() => {
    populateDonationRequest();
  }, [])

  function handleProductQuantityChange(idx, evt) {
    const newProducts = requiredProducts.map((p, pidx) => {
      if (idx !== pidx) return p;

      return {...p, quantity: evt.target.value};
    })

    setRequiredProducts(newProducts)
  }

  return (    
    <Card>
      {donationRequest && (
        <div className='mt-4 mx-5'>
          <h3 className='text-center'>
            Formular donatie
          </h3>
          <p className='mt-5'><strong>Administrator centru:</strong> {donationRequest.centerName}</p>
          <p><strong>Numar de telefon:</strong> {donationRequest.centerPhoneNumber}</p>
          <p>
            <strong>Adresa:</strong> {donationRequest.address.city}, strada {donationRequest.address.street}, 
            numarul {donationRequest.address.number}, {donationRequest.address.country}.
          </p>
          <p><strong>Statusul comenzii:</strong> {donationRequest.status}, termen limita {donationRequest.availableTo}</p>
        
          <ul>
          {requiredProducts.map((p, i) =>
            <li key={p.requestProductId} className='mb-2'>
              <input
                type='number'
                min='0'
                value={p.quantity}
                onChange={(e) => handleProductQuantityChange(i, e)}
              />
              <span> {p.measureUnit} {p.productName}</span>
            </li>
          )}
          </ul>
          <Button color="primary" className='mb-4' onClick={makeDonation}>Doneaza!</Button>
          <Alert className='mb-4' color="success" isOpen={isSuccess} >
            Modificarile au avut loc cu succes. Un curier va va contacta pentru a prelua produsele 
            in cel mai scurt timp.
          </Alert>
        </div>
      )}
    </Card>
  );

  async function populateDonationRequest() {
    const accessToken = await getAccessTokenSilently();
    axiosInstance.get(`donation/${donationRequestId}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(({ data }) => {
        setDonationRequest(data);
        setRequiredProducts(data.productsRequired);
      });
  }

  async function makeDonation() {
    const accessToken = await getAccessTokenSilently();
    const param = { 
      donorUserId: userData.userId, 
      donatedProducts: requiredProducts 
    };
    axiosInstance.post('donation/make-donation', param, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }).then(() => {
      setIsSuccess(true);
    });
  }
}

export default withAuthenticationRequired(Donate, {
  onRedirecting: () => <Loading />,
});
