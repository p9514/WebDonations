import React, { useState, useEffect } from "react";
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import Profile from './components/Profile';
import DonationRequests from './components/DonationRequests';
import Donate from "./components/Donate";
import CreateDonationRequest from './components/CreateDonationRequest';
import { userContext } from "./config/userContext";
import { useAuth0 } from "@auth0/auth0-react";
import axiosInstance from "./config/Axios";

import './custom.css'


const App = () => {
  const [userData, setUserData] = useState();
  const { user } = useAuth0();

  useEffect(() => {
    if (user) {
      axiosInstance.get(`user/${user.email}`)
      .then(({ data }) => {
        setUserData(data);
      });
    }
    
  }, [user]);

  return (
    <userContext.Provider value={userData}>
      <Layout>
        <Route exact path='/' component={Profile} />
        { (userData && userData.role === "Donations Center") &&
          (<Route path='/createDonation' component={CreateDonationRequest} />)
        }
        <Route path='/requests' component={DonationRequests} />
        <Route path='/donate/:donationRequestId' component={Donate} />
      </Layout>
    </userContext.Provider>
  );  
}

export default App;
