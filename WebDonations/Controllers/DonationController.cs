﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Models.Donations;
using Prometheus;

namespace WebDonations.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DonationController : ControllerBase
    {
        private readonly IDonationsService _donationsService;
        private readonly IMetricReporter _metricReporter;

        public DonationController(IDonationsService donationService, IMetricReporter metricReporter)
        {
            _donationsService = donationService;
            _metricReporter = metricReporter;
        }

        [HttpGet]
        public IEnumerable<DonationRequestDetails> GetDonationRequests()
        {
            return _donationsService.GetDonationRequests();
        }

        [HttpGet("{id}")]
        public DonationRequestDetails GetDonationRequest(long id)
        {
            return _donationsService.GetDonationRequest(id);
        }

        [HttpGet("center/{centerUserId}")]
        public IEnumerable<DonationRequestDetails> GetDonationRequests(long centerUserId)
        {
            return _donationsService.GetCenterDonationRequests(centerUserId);
        }

        [HttpPost]
        public void PostDonationRequest(DonationRequestDetailsRequest req)
        {
            _donationsService.PostDonationRequest(req);
            _metricReporter.RegisterDonationRequest();
        }

        [HttpPost("update")]
        public void UpdateDonationRequest(UpdateDonationRequest req)
        {
            _donationsService.UpdateDonationRequest(req);
        }

        [HttpPost("make-donation")]
        public void DonateProducts(DonateProductsRequest req)
        {
            _donationsService.DonateProducts(req);
            _metricReporter.RegisterDonation();
        }
    }
}
