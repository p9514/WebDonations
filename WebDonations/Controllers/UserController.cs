﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Models.Users;

namespace WebDonations.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UserController(IUsersService userService)
        {
            _usersService = userService;
        }

        [HttpGet]
        public IEnumerable<UserModel> Index()
        {
            return _usersService.Index();
        }

        [HttpGet("{userId}/profile")]
        public UserDetails GetUserProfile(long userId)
        {
            return _usersService.GetUserProfile(userId);
        }

        [HttpPost]
        public void UpdateUserProfile(UpdateUserRequest req)
        {
            _usersService.UpdateUserProfile(req);
        }

        [HttpGet("{userEmail}")]
        public UserContext GetUserContext(string userEmail)
        {
            return _usersService.GetUserContext(userEmail);
        }
    }
}
