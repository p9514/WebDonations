﻿namespace Helpers.Configurations;

public class RabbitMQConfiguration
{
    public string? HostName { get; set; }
}
