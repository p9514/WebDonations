﻿using System.ComponentModel;

namespace Helpers.Enums;

public static class EnumExtensions
{
    public static string GetEnumDescription(this UserRole enumValue)
    {
        var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

        var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

        return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
    }

    public static T AsEnum<T>(this int value) where T : struct
    {
        if (!(typeof(T).IsEnum))
        {
            throw new ArgumentException("T must be an enumerated type");
        }

        return (T)Enum.Parse(typeof(T), value.ToString());
    }
}
