﻿using System.ComponentModel;

namespace Helpers.Enums;

public enum UserRole
{
    [Description("Donor")]
    Donor = 1,

    [Description("Donations Center")]
    Center = 2,
}
