﻿using Helpers.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Reflection;

#nullable disable

namespace Database.Entities;

public partial class WDDbContext : DbContext
{
    private readonly ConnectionStrings _connectionSettings;

    public WDDbContext()
    {
    }

    public WDDbContext(DbContextOptions<WDDbContext> options, IOptions<ConnectionStrings> connectionStrings)
        : base(options)
    {
        _connectionSettings = connectionStrings.Value;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseMySQL(_connectionSettings.DefaultConnection);
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}
