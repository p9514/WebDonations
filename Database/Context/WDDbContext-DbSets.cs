﻿using Microsoft.EntityFrameworkCore;

namespace Database.Entities;

public partial class WDDbContext
{
    public virtual DbSet<DonationRequest> DonationRequests { get; set; }
    public virtual DbSet<DonationRequestProduct> DonationRequestProducts { get; set; }
    public virtual DbSet<User> Users { get; set; }
    public virtual DbSet<UserAddress> UserAddresses { get; set; }
    public virtual DbSet<UserDonationProduct> UserDonationProducts { get; set; }
}
