﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Database.Entities;

public partial class User
{
    public User()
    {
        DonationRequests = new HashSet<DonationRequest>();
        UserAddresses = new HashSet<UserAddress>();
        UserDonationProducts = new HashSet<UserDonationProduct>();
    }

    public long IdUser { get; set; }
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Phone { get; set; }
    public int RoleId { get; set; }

    public virtual ICollection<DonationRequest> DonationRequests { get; set; }
    public virtual ICollection<UserAddress> UserAddresses { get; set; }
    public virtual ICollection<UserDonationProduct> UserDonationProducts { get; set; }
}
