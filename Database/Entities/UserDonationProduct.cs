﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Database.Entities;

public partial class UserDonationProduct
{
    public long IdUserDonationProduct { get; set; }
    public long IdDonorUser { get; set; }
    public long IdRequestProduct { get; set; }
    public int Quantity { get; set; }

    public virtual User DonorUser { get; set; }
    public virtual DonationRequestProduct IdRequestProductNavigation { get; set; }
}
