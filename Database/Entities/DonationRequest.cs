﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Database.Entities;

public partial class DonationRequest
{
    public DonationRequest()
    {
        DonationRequestProducts = new HashSet<DonationRequestProduct>();
    }

    public long IdRequest { get; set; }
    public long IdCenterUser { get; set; }
    public string Status { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime AvailableTo { get; set; }

    public virtual User CenterUser { get; set; }
    public virtual ICollection<DonationRequestProduct> DonationRequestProducts { get; set; }
}
