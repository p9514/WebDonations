﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Database.Entities;

public partial class DonationRequestProduct
{
    public DonationRequestProduct()
    {
        UserDonationProducts = new HashSet<UserDonationProduct>();
    }

    public long IdRequestProduct { get; set; }
    public long IdRequest { get; set; }
    public string ProductName { get; set; }
    public long Quantity { get; set; }
    public string MeasureUnit { get; set; }

    public virtual DonationRequest Request { get; set; }
    public virtual ICollection<UserDonationProduct> UserDonationProducts { get; set; }
}
