﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Database.Entities;

public partial class UserAddress
{
    public long IdAddress { get; set; }
    public long? IdUser { get; set; }
    public string Country { get; set; }
    public string City { get; set; }
    public string Street { get; set; }
    public string Number { get; set; }

    public virtual User User { get; set; }
}
