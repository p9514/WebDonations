﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations;

public class UsersConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> entity)
    {
        entity.HasKey(e => e.IdUser)
                .HasName("PRIMARY");

        entity.Property(e => e.Email)
            .IsRequired()
            .HasMaxLength(255);

        entity.Property(e => e.FirstName)
            .IsRequired()
            .HasMaxLength(255);

        entity.Property(e => e.LastName)
            .IsRequired()
            .HasMaxLength(255);

        entity.Property(e => e.Phone).HasMaxLength(15);

        entity.Property(e => e.RoleId).HasDefaultValueSql("'1'");
    }
}
