﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations;

public class UserDonationProductsConfiguration : IEntityTypeConfiguration<UserDonationProduct>
{
    public void Configure(EntityTypeBuilder<UserDonationProduct> entity)
    {
        entity.HasKey(e => e.IdUserDonationProduct)
                .HasName("PRIMARY");

        entity.HasIndex(e => e.IdRequestProduct, "DonationProduct_To_RequestProduct_FK_idx");

        entity.HasIndex(e => e.IdDonorUser, "DonationProduct_To_User_FK_idx");

        entity.HasOne(d => d.DonorUser)
            .WithMany(p => p.UserDonationProducts)
            .HasForeignKey(d => d.IdDonorUser)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("DonationProduct_To_User_FK");

        entity.HasOne(d => d.IdRequestProductNavigation)
            .WithMany(p => p.UserDonationProducts)
            .HasForeignKey(d => d.IdRequestProduct)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("DonationProduct_To_RequestProduct_FK");
    }
}
