﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations;

public class DonationRequestsConfiguration : IEntityTypeConfiguration<DonationRequest>
{
    public void Configure(EntityTypeBuilder<DonationRequest> entity)
    {
        entity.HasKey(e => e.IdRequest)
                .HasName("PRIMARY");

        entity.HasIndex(e => e.IdCenterUser, "Request_to_User_FK_idx");

        entity.Property(e => e.Status)
            .IsRequired()
            .HasMaxLength(45);

        entity.HasOne(d => d.CenterUser)
            .WithMany(p => p.DonationRequests)
            .HasForeignKey(d => d.IdCenterUser)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("Request_to_User_FK");
    }
}
