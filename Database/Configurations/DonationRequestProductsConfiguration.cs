﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations;

public class DonationRequestProductsConfiguration : IEntityTypeConfiguration<DonationRequestProduct>
{
    public void Configure(EntityTypeBuilder<DonationRequestProduct> entity)
    {
        entity.HasKey(e => e.IdRequestProduct)
                .HasName("PRIMARY");

        entity.HasIndex(e => e.IdRequest, "RequestProduct_to_Request_FK_idx");

        entity.Property(e => e.MeasureUnit).HasMaxLength(45);

        entity.Property(e => e.ProductName)
            .IsRequired()
            .HasMaxLength(255);

        entity.HasOne(d => d.Request)
            .WithMany(p => p.DonationRequestProducts)
            .HasForeignKey(d => d.IdRequest)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("RequestProduct_to_Request_FK");
    }
}
