﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Configurations;

public class UserAddressesConfiguration : IEntityTypeConfiguration<UserAddress>
{
    public void Configure(EntityTypeBuilder<UserAddress> entity)
    {
        entity.HasKey(e => e.IdAddress)
                .HasName("PRIMARY");

        entity.HasIndex(e => e.IdUser, "From_Address_To_User_FK_idx");

        entity.Property(e => e.City)
            .IsRequired()
            .HasMaxLength(255);

        entity.Property(e => e.Country)
            .IsRequired()
            .HasMaxLength(255);

        entity.Property(e => e.Number).HasMaxLength(45);

        entity.Property(e => e.Street)
            .IsRequired()
            .HasMaxLength(255);

        entity.HasOne(d => d.User)
            .WithMany(p => p.UserAddresses)
            .HasForeignKey(d => d.IdUser)
            .HasConstraintName("From_Address_To_User_FK");
    }
}
